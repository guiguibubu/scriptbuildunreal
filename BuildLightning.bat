echo %UNREAL_ENGINE_CMD%
echo %PROJECT_FILE%


echo =================================================================
echo On build les lumieres
echo =================================================================

attrib -r %PROJECT_DIR%\Content\* /s
call "%UNREAL_ENGINE_CMD%" "%PROJECT_FILE%" -run=resavepackages -buildlighting -BuildHLOD -quality=Preview -allowcommandletrendering -MAP=/Game/Maps/GameMap_Final+/Game/Maps/Taverne

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End