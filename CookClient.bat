echo =================================================================
echo On cook le projet (Client) ...
echo =================================================================

call %SCRIPT_BUILD_UE% BuildCookRun -project=%PROJECT_FILE%^
 -noP4 -platform=Win64 -allmaps -build -clientconfig=Development^
 -cook -stage -archive -archivedirectory=%DEST_DIR_BUILD%\Client

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End