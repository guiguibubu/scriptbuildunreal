echo =================================================================
echo On compile la solution VisualStudio
echo =================================================================

call %SCRIPT_COMPILE_VS% %PROJECT_FILE_SLN%^
 /t:build /p:Platform=Win64;verbosity=diagnostic

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End