echo =================================================================
echo On copie les videos dans le repertoire de Build
echo =================================================================

set DEST_DIR_VIDEO=%DEST_DIR_BUILD%\Client\WindowsNoEditor\battle_for_pandora\Content\Video

echo %DEST_DIR_VIDEO%

:: Prepare le dossier de destination
if exist %DEST_DIR_VIDEO% rmdir /S /Q %DEST_DIR_VIDEO%
mkdir %DEST_DIR_VIDEO%

if %ERRORLEVEL% NEQ 0 (
    goto End
)

robocopy %PROJECT_DIR%\Content\Video %DEST_DIR_VIDEO% /E

IF %ERRORLEVEL% LEQ 1 (
	SET ERRORLEVEL = 0
	echo "Succes de la copie"
)

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End