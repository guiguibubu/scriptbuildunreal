echo =================================================================
echo On valorise les variables
echo =================================================================


set ENGINE_DIR=D:\UE_4.21\Engine
set PROJECT_DIR=D:\VioletMurder\battleforpangoracode3
set PROJECT_FILE=%PROJECT_DIR%\battle_for_pandora.uproject
set PROJECT_FILE_SLN=%PROJECT_DIR%\battle_for_pandora.sln

set DEST_DIR_BUILD=%PROJECT_DIR%\..\Builds\%CURRENT_DATE%\
set LATEST_LINK_BUILD=%PROJECT_DIR%\..\Latest

set SCRIPT_GENERATION_VS=%ENGINE_DIR%\Binaries\DotNET\UnrealBuildTool.exe
set SCRIPT_COMPILE_VS=%ENGINE_DIR%\Build\BatchFiles\MSBuild.bat
set SCRIPT_BUILD_UE=%ENGINE_DIR%\Build\BatchFiles\RunUAT.bat
set UNREAL_ENGINE=%ENGINE_DIR%\Binaries\Win64\UE4Editor.exe
set UNREAL_ENGINE_CMD=%ENGINE_DIR%\Binaries\Win64\UE4Editor-Cmd.exe