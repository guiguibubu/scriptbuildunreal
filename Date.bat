echo =================================================================
echo On recupere le Timestamp
echo =================================================================

echo Fetching current date...
for /f "tokens=*" %%a in ('powershell get-date -format "{dd-MM-yyyy_hhmmss}"') do set CURRENT_DATE=%%a
