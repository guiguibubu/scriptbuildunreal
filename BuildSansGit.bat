@echo off

%~d0
cd %~dp0

::====================================================================
::====================================================================
:: Les adresse des dossiers et fichiers
::====================================================================
::====================================================================
call NomDossierFichier.bat
::====================================================================
::====================================================================
:: RÉCUPÈRE LES DERNIERS CHANGEMENTS DU SERVEUR
::====================================================================
::====================================================================
::call RecupereGit.bat
::====================================================================
::====================================================================
::====================================================================
:: BUILD SOLUTION VS
::====================================================================
::====================================================================
call GenerationSolutionVS.bat
if %ERRORLEVEL% NEQ 0 (
    goto End
)

call CompilationSolutionVS.bat
if %ERRORLEVEL% NEQ 0 (
    goto End
)
::====================================================================
::====================================================================
::====================================================================
:: BUILD PROJET UNREAL
::====================================================================
::====================================================================

echo Preparing destination directory

:: Prepare le dossier de destination
if exist %DEST_DIR_BUILD% rmdir /S /Q %DEST_DIR_BUILD%
mkdir %DEST_DIR_BUILD%

call CookClient.bat
if %ERRORLEVEL% NEQ 0 (
    goto End
)

::call CookServer.bat
if %ERRORLEVEL% NEQ 0 (
    goto End
)

echo =================================================================
echo 					Build finished !!!
echo =================================================================

echo %LOCAL_REV% > %DEST_DIR_BUILD%\git_hash.txt

:: Crée un lien symbolique vers le dernier build
if exist %LATEST_LINK_BUILD% rmdir %LATEST_LINK_BUILD%
mklink /D %LATEST_LINK_BUILD% %DEST_DIR%

if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End