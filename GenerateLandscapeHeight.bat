echo =================================================================
echo On genere la LookUpTable
echo =================================================================

call "%UNREAL_ENGINE_CMD%" "%PROJECT_FILE%" /Game/Maps/MapLookUpTable?game=/Script/battle_for_pandora.LandscapeHeightGeneratorGM -server -log

if %ERRORLEVEL% NEQ 0 (
    goto End
)

COPY /B %PROJECT_DIR%\Content\LandscapeHeights /B %DEST_DIR_BUILD%\Client\WindowsNoEditor\battle_for_pandora\Content /V


if %ERRORLEVEL% NEQ 0 (
    goto End
)

:End